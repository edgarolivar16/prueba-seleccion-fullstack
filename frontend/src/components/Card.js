import React from "react";
import { Link } from "react-router-dom";
import GenericImg from "./GenericImg"

export default ({ id, name, house, poster }) => {


  return (
    <Link to={`/detail/${id}`}>
      <div className="card cursorPointer">
        <div className="card-image">
          <figure className="image">
            {
              poster ? 
              <img src={poster} alt={name} />
              // <img src={poster ? poster : generic} alt={name} />
              :
              <GenericImg name={name}></GenericImg>
            }
          </figure>
        </div>
        <div className="card-content">
          <div className="media">
            <div className="media-content">
              <p className="title is-4">{name}</p>
              <p className="subtitle is-6">{house}</p>
            </div>
          </div>
        </div>
      </div>
    </Link>
  );
};
