import React from "react";
import Card from "./Card";

export default ({ characters }) => {
  return (
    <div className="CharactersList">
      {characters.map((c) => (
        <div className="CharactersList-item" key={c._id}>
          <Card
            id={c._id}
            name={c.name}
            house={c.house}
            poster={c.image}
          ></Card>
        </div>
      ))}
    </div>
  );
};
