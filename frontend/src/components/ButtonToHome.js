import React from "react";
import {Link} from "react-router-dom"

export default () => (
  <Link to="/" className="button is-info">Go back</Link>
)
