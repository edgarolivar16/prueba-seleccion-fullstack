import Title from './Title'
import SearchForm  from './SearchForm'
import CharactersList from './CharactersList'
import ButtonToHome from "./ButtonToHome";
import GenericImg from "./GenericImg"

export { ButtonToHome, Title, SearchForm, CharactersList, GenericImg};
