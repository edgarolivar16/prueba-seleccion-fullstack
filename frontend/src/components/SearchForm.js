import React, { useState } from "react";

export default ({ onResults }) => {
  const [inputChar, setInputChar] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (inputChar) {
      onResults(inputChar);
    }
  };

  const handleChange = (e) => {
    setInputChar(e.target.value);
  };

  const handleKey = (e) => {
    if(e.keyCode === 13) {
      onResults(document.querySelector("#inputForm").value)
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="field has-addons">
        <div className="control">
          <input
            onChange={handleChange}
            className="input"
            type="text"
            placeholder="Jon Snow"
            onKeyUp={handleKey}
            id="inputForm"
          ></input>
        </div>
        <div className="control">
          <button className="button is-info">Search</button>
        </div>
      </div>
    </form>
  );
};
