import React from "react";
import "./App.css";
import "bulma/css/bulma.css";
import { Home, NotFound, Detail } from "./pages";
import { Switch, Route } from "react-router-dom";
global.BASE_URL = "http://localhost:3000"

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Home}></Route>
        <Route exact path="/detail/:id" component={Detail}></Route>
        <Route component={NotFound}></Route>
      </Switch>
    </div>
  );
}

export default App;
