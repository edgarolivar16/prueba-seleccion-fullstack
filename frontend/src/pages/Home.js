import React, { useState, useEffect } from "react";
import { Title, SearchForm, CharactersList } from "../components";

export default () => {
  const [result, setResult] = useState([]);
  const [skip, setSkip] = useState(1);
  const [filter, setFilter] = useState("");
  const size = 10;
  
  const nextPage = () => {
    setSkip(skip + 1);
  };

  const previousPage = () => {
    if (skip > 1) setSkip(skip - 1);
  };

  const handleResults = async (word) => {
    setFilter(word);
    setSkip(1);
    let rawData = await fetch(
      `${global.BASE_URL}/character?filter=${filter}&page=${skip}&size=${size}`
    );
    rawData = await rawData.json();
    console.log(rawData);
    setResult(rawData.characters);
  };

  const renderResult = () => {
    return (
      <>
        <CharactersList characters={result}></CharactersList>
        <div className="box buttons is-centered">
          {skip === 1 ? null : (
            <button className="button" onClick={previousPage}>
              Previous Page
            </button>
          )}
          {
            result.length < 10 ? null : 
              <button className="button" onClick={nextPage}>
                Next Page
              </button>
          }
        </div>
      </>
    );
  };
  useEffect(() => {
    fetch(
      `${global.BASE_URL}/character?filter=${filter}&page=${skip}&size=${size}`
    )
      .then((res) => res.json())
      .then((res) => {
        if (res.ok) {
          setTimeout(() => setResult(res.characters), 1000);
        } else {
          alert("Problemas para conectar con api!");
        }
      });
  }, [skip, filter]);

  return (
    <div>
      <Title>Search Character!</Title>
      <div className="SearchForm-wrapper">
        <SearchForm onResults={handleResults}></SearchForm>
      </div>
      <small>Use the form to search a Character from GOT!</small>
      {result.length > 0 ? renderResult() : <p>Cargando...</p>}
      <small>Made with ❤ and cafecito by <a href="https://github.com/The3dgar">The3dgar</a></small>

    </div>
  );
};
