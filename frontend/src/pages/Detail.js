import React, { useState, useEffect } from "react";
import { ButtonToHome, GenericImg } from "../components";


const Detail = ({ match }) => {
  const [character, setCharacter] = useState({});
  const [loading, setLoading] = useState(true);
  const { id } = match.params;

  useEffect(() => {
    fetch(`${global.BASE_URL}/character/${id}`)
      .then((res) => res.json())
      .then((res) => {
        setCharacter(res.character);
        setLoading(false);
      });
  }, [id]);
  console.log(character)
  return (
    <div>
      <ButtonToHome></ButtonToHome>
      {loading ? (
        <p>Cargando...</p>
      ) : (
        <>
          <h1>Name: {character.name}</h1>
          <h1>Titles: {character.titles.join(", ")}</h1>
          {
            character.image ? 
            <img src={character.image} alt="character img"></img>
            :
            <GenericImg name={character.name}></GenericImg>

          }
          <h3>Slug: {character.slug || "None"}</h3>
          <h3>Gender: {character.gender}</h3>
          <h3>House: {character.house}</h3>
          <h3>Rank: {character.pagerank.rank}</h3>
          <h3>Books: {character.books.join(", ")}</h3>
          
        </>
      )}
    </div>
  );
};

export default Detail;
