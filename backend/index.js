require("./config/config");
const express = require("express");
const app = express();
var cors = require('cors')
app.use(cors())

// habilitar la carpeta frontend
// app.use("/", express.static(path.join(__dirname, 'frontend')))

// configuracion general de rutas
app.use(require('./routes/index'))

app.listen(process.env.PORT, () => {
  console.log(`Server on port: ${process.env.PORT}`);
});
