const mongoose = require("mongoose")
const Character = require("../models/characters");

const getAll = async (query) => {
  const page = parseInt(query.page) || 1
  const size = parseInt(query.size) || 10
  const filter = query.filter || ""

  return await Character.find({$or: [{name: {$regex: filter}},{ house: {$regex: filter}}] })
    .skip((page - 1) * size)
    .limit(size);
};

const getById = async (id) => {
  if(!mongoose.Types.ObjectId.isValid(id) ) return false
  return await Character.findById(id)
}

module.exports = {
  getAll,
  getById
};
