const fetch = require("node-fetch");
const Character = require("../models/characters");
require("../config/config");

const getAllCaracters = async () => {
  console.log("Llamando a api-got...");
  let data = await fetch("https://api.got.show/api/book/characters");
  
  console.log("Procesando los datos...");
  let characters = await data.json();
  characters = characters.map((c) => new Character(c));
  Character.create(characters)
    .then(console.log("DB cargada con los personajes!"))
    .catch((e) => {
      console.log("Error cargando los personajes");
      console.log(e);
    });
};

getAllCaracters();
