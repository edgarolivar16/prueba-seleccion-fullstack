const express = require("express");
const app = express();
const Character = require("../services/characters");

app.get("/", async (req, res) => {
  const characters = await Character.getAll(req.query);
  res.json({
    ok: true,
    characters,
  });
});

app.get("/:id", async (req, res) => {
  const id = req.params.id;
  const character = await Character.getById(id);
  if (!character)
    return res.json({
      ok: false,
      message: "invalid Id",
    });

  res.json({
    ok: true,
    character
  });
});

module.exports = app;
