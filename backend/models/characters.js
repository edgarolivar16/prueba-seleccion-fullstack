const mongoose = require("mongoose");
let Schema = mongoose.Schema;

let characterSchema = new Schema(
  {
    titles: [
      {
        type: String,
      },
    ],
    origin: [
      {
        type: String,
      },
    ],
    books: [
      {
        type: String,
      },
    ],
    name: {
      type: String,
      unique: true
    },
    image: {
      type: String,
    },
    gender: {
      type: String,
    },
    house: {
      type: String,
    },
    first_seen: {
      type: String,
    },
    actor: {
      type: String,
    },
    slug : {
      type: String
    },
    pagerank: {
      title: {
        type: String
      },
      rank: {
        type: Number
      }
    }
  },
  { versionKey: false }
);

module.exports = mongoose.model("Character", characterSchema);
